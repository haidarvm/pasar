<?php

// function loadDb() {
// $CI =& get_instance();
// $CI->load->library('database');
// return $CI;
// }
function checkRes($query) {
    // $CI =& get_instance();
    // $CI->load->library('database');
    if ($query->num_rows() > 0) {
        return $query->result();
    } else {
        return false;
    }
}

function checkRow($query) {
    // $CI =& get_instance();
    // $CI->load->library('database');
    if ($query->num_rows() > 0) {
        return $query->row();
    } else {
        return false;
    }
}

function insertID() {
    return $this->db->insert_id();
}

function numberOnly($num){
    return preg_replace('/\D/', '', $num);
}

function fire($log) {
    $ci = & get_instance();
    $ci->load->library('firephp');
    return $ci->firephp->log($log, __METHOD__);
}

function uploader($log) {
    $ci = & get_instance();
    $ci->load->library('uploadhandler');
    return $ci->uploadhandler->log($log, __METHOD__);
}

function slugify($text) {
    // replace non letter or digits by -
    $text = preg_replace('~[^\\pL\d]+~u', '-', $text);
    // trim
    $text = trim($text, '-');
    // transliterate
    $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
    // lowercase
    $text = strtolower($text);
    // remove unwanted characters
    $text = preg_replace('~[^-\w]+~', '', $text);
    if (empty($text)) {
        return 'n-a';
    }
    return $text;
}

function prod_thumb_dir() {
    return base_url() . 'assets/product_thumb/';
}

function prod_dir() {
    return base_url() . 'assets/product/';
}

function basic_path() {
    $fr_loc = explode('/', $_SERVER['SCRIPT_NAME']);
    $base_path = $_SERVER['DOCUMENT_ROOT'] . '/' . $fr_loc[1] . '/';
    return $base_path;
}
/*
 For Mobile Json data purposes
 http://stackoverflow.com/questions/15485354/angular-http-post-to-php-and-undefined
*/
function server_ori(){
    
    header('Access-Control-Allow-Headers: Content-Type');
    header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
    header('Access-Control-Allow-Origin: *');
    header('Access-Control-Allow-Credentials: true');

    /*if (isset($_SERVER['HTTP_ORIGIN'])) {
        header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
        header('Access-Control-Allow-Credentials: true');
        header('Access-Control-Max-Age: 46400');    // cache for 1 day
    }

        // Access-Control headers are received during OPTIONS requests
    if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers:
        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
        exit(0);
    }*/
}