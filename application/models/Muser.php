<?php

/*
 * By Haidar Mar'ie Email = haidarvm@gmail.com MProduct
 */
class MUser extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function getAllProduct() {

    }

    function getAllCategories() {

    }

    function insertUser($data) {
       $this->db->insert("user",$data);
       return $this->db->insert_id();
    }

    function updateUser($data,$user_id) {
       $this->db->update("user", $data, array('user_id'=>$user_id));
       return $user_id;
    }

    function checkUserId($user_id){
        $query = $this->db->get_where("user", array('user_id'=>$user_id));
        //echo $this->db->last_query();
        return checkRow($query);
    }

}
