<?php
if (! defined('BASEPATH'))
    exit('No direct script access allowed');

class Product extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->mcategory = new MCategory();
        $this->mproduct = new MProduct();
    }

    public function index() {
        $this->product_list();
    }

    public function create() {
        $data['title'] = "Admin Create Product";
        $data['action'] = 'add';
        $data['productDraft'] = $this->mproduct->getProductDraft();
        if (! $data['productDraft']) {
            $dataDraft['name'] = 'draft';
            $insertProductId = $this->mproduct->insertQuickProduct($dataDraft);
            $dataImgDraft['product_id'] = $insertProductId;
            $dataImgDraft['name'] = 'draft';
            // $insertProductImageId = $this->mproduct->insertQuickProductImg($dataImgDraft);
            $data['productDraft'] = $this->mproduct->getLatestProductDraft($insertProductId);
            $data['productImgDraft'] = null;
        }
        $data['productImgDraft'] = $this->mproduct->getProductImgDraft($data['productDraft']->product_id);
        $data['categories'] = $this->mcategory->getAllCategories();
        $data['units'] = $this->mcategory->getAllUnit();
        $this->load->admin_template('admin/product', $data);
    }

    public function product_list() {
        $data['title'] = "Admin Product List";
        $data['getAll'] = $this->mproduct->getAllProduct();
        $this->load->admin_template('admin/product_list', $data);
    }

    public function update($id=NULL) {
        $data['title'] = "Admin Edit Product";
        $data['action'] = 'update';
        $post = $this->input->post();
        if($post) {
            print_r($post);
            $this->mproduct->updateProduct($post,$post['product_id']);
            redirect(site_url().'admin/product/product_list');
        } else {
            $data['product'] = $this->mproduct->getProduct($id);
            $data['productImg'] = $this->mproduct->getProductImgDraft($id);
            $data['categories'] = $this->mcategory->getAllCategories();
            $data['units'] = $this->mcategory->getAllUnit();
            $this->load->admin_template('admin/product', $data);
        }
    }

    public function blank() {
        $data['title'] = "Admin Blank";
        $this->load->admin_template('admin/blank', $data);
    }
    
    public function insert_quick_product() {
        $post = $this->input->post();
        unset($post['_wysihtml5_mode']);
        $post['slug'] = $this->checkSlug($post['name']);
        $post['price_id'] = $this->mproduct->insertProductPrice($post);
        $this->mproduct->editProduct($post, $post['product_id']);
        $this->insert_draft_product();
    }

    public function insert_draft_product() {
        $post = $this->input->post();
        $dataDraft['name'] = 'draft';
        $product_id = $this->mproduct->insertQuickProduct($dataDraft);
        $dataImgDraft['product_id'] = $product_id;
        $dataImgDraft['name'] = 'draft';
        // $image_id = $this->mproduct->insertQuickProductImg($dataImgDraft);
        $data['product_id'] = $product_id;
        $data['image_id'] = null;
        echo json_encode($data);
    }
    
    public function checkSlug($slug) {
        $slugify = slugify($slug);
        $checkSlug = $this->mproduct->checkSlug($slug);
        if ($checkSlug) {
            return $checkSlug;
        } else {
            return $slugify;
        }
    }

}
