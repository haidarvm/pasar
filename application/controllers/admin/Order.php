<?php
if (! defined('BASEPATH'))
    exit('No direct script access allowed');

class Order extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->mcategory = new MCategory();
        $this->mproduct = new MProduct();
        $this->morder = new MOrder();
    }

    public function index() {
        $this->get_all_new();
    }

    public function get_all_new() {
        $data['title'] = "Admin Order New";
    	$data['getAll'] = $this->morder->getAllNewOrder();
        $this->load->admin_template('admin/order', $data);
    }

    public function get_all_done() {
        $data['title'] = "Admin Order Done";
    	$data['getAll'] = $this->morder->getAllDoneOrder();
        $this->load->admin_template('admin/order', $data);
    }

    public function detail($order_id) {
        $data['title'] = "Admin Order Details";
    	$data['allOrder'] = $this->morder->getOrderItem($order_id);
        $this->load->admin_template('admin/order_details', $data);
    }

}