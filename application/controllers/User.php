<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('muser');
		$this->muser = new MUser();
	}

	public function index() {
		//echo 'test';
		$this->get_all();
	}

	public function get_all() {
		echo 'all';
	}

	public function insert() {
		if (isset($_SERVER['HTTP_ORIGIN'])) {
			header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
			header('Access-Control-Allow-Credentials: true');
			header('Access-Control-Max-Age: 86400');    // cache for 1 day
		}

		// Access-Control headers are received during OPTIONS requests
		if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
			if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
			header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
			if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
			header("Access-Control-Allow-Headers:
			{$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
			exit(0);
		}
		$postdata = file_get_contents("php://input");
		if (isset($postdata)) {
			$ionPost = json_decode($postdata);
			//print_r($postdata);

			if(!empty($ionPost->full_name) && $ionPost->full_name !== "undefined") {
				$data['full_name'] = $ionPost->full_name;
				$data['address'] = $ionPost->address;
				$data['phone'] = $ionPost->phone;
				$data['email'] = $ionPost->email;
				if(!$this->muser->checkUserId($ionPost->user_id)){
					//echo 'blun ada';
					$json_data['user_id']  = $this->muser->insertUser($data);
				}elseif(!empty($ionPost->user_id) && $ionPost->user_id !== "undefined") {
					$json_data['user_id']  = $this->muser->updateUser($data, $ionPost->user_id);
				} else {
					$json_data['user_id']  = $this->muser->insertUser($data);
				}
				echo json_encode($json_data);
			}
		}
	}
}
